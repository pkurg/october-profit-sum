// ==UserScript==
// @name New Script
// @namespace MeddleMonkey Scripts
// @grant none
// @include https://octobercms.com/account/author/reports
// ==/UserScript==


window.getSum = function() {
var data = Array();

$("table tr").each(function(i, v){
    data[i] = Array();
    $(this).children('td').each(function(ii, vv){
        data[i][ii] = $(this).text();
    }); 
})

var rows = data.length;

var sum = 0;

for (let i = 1; i < rows; i++) { 
  
	cur = data[i][8];  	
	cur = cur.replace('$','');
	sum = sum + parseFloat(cur);
  //	console.log(cur);

}

//console.log(sum);

sum = sum.toFixed(2);

//$( "table" ).after( document.createTextNode( "Total: " + sum ) );

$( "table" ).after('<p id="total" style=" text-align: right;font-weight: bold;">Total profit: ' + sum + '</p>');

}


var t=setInterval(runFunction,2000);


function runFunction() {
	
	if (window.location.href == "https://octobercms.com/account/author/reports") {

		
		if ( $( "#total" ).length == 0 ) {

		//$("table").append($('<input id="gridEdit" type="button" onclick="getSum()" value="Total"/>'));
		getSum();

		}

	}

}



